
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class TerrainFace 
{

    private float planetRadius;

    Mesh mesh;

    Gradient gradient;

    int resolution;
    ItemSpawner spawner;

    GameObject itemsParent;


    Vector3 localUp;

    static float minHeight = float.MaxValue;
    static float maxHeight = float.MinValue;

    Vector3 axisB;
    Vector3 axisA;

    OctaveInformation[] octaves;

    ItemSpawner itemsSpawner;

    Vector3[] vertices;
    float[] verticesHeight;
    int[] tringles;

    public TerrainFace(Mesh mesh, int resolution, Vector3 localUp,  float planetRadius, OctaveInformation[] octaves, Gradient gradient,ItemSpawner itemSpawner,GameObject itemsParent)
    {
        this.mesh = mesh;
        this.resolution = resolution;
        this.localUp = localUp;
        this.planetRadius = planetRadius;
        this.octaves = octaves;
        this.gradient = gradient;
        this.itemsSpawner = itemSpawner;
        this.itemsParent = itemsParent;

        axisA = new Vector3(localUp.y, localUp.z, localUp.x);
        axisB = Vector3.Cross(localUp, axisA);
        
    }

    public void ConstructVertices()
    {
        vertices = new Vector3[resolution * resolution];
        

        verticesHeight = new float[resolution * resolution];

        tringles = new int[(resolution - 1)*(resolution - 1) * 2 * 3];
        int triIndex = 0;



        for (int y = 0; y < resolution; y++)
        {
            for (int x = 0; x < resolution; x++)
            {
                


                int i = x + y * resolution;
                Vector2 percent = new Vector2(x, y) / (resolution - 1);

                Vector3 pointOnUnitCube = localUp + (percent.x - .5f) * 2 * axisA + (percent.y - .5f) * 2 * axisB;
                Vector3 pointOnUnitSphere = pointOnUnitCube.normalized;

                float elevation = 0;
                float firstLayerValue = 0;

                

                if (octaves.Length > 0)
                {
                    firstLayerValue = NoiseEvaluate(pointOnUnitSphere, octaves[0]);
                    if (octaves[0].enabled)
                    {
                        elevation = firstLayerValue;
                    }
                }


                for (int octaveCount = 1; octaveCount < octaves.Length; octaveCount++)
                {
                    if (octaves[octaveCount].enabled)
                    {
                        float mask = (octaves[octaveCount].useFirstLayerAsMask) ? firstLayerValue : 1;
                        elevation += NoiseEvaluate(pointOnUnitSphere, octaves[octaveCount]) * mask;
                    }
                }
                

                if (elevation > maxHeight)
                {
                    maxHeight = elevation;
                }
                if (elevation < minHeight)
                {
                    minHeight = elevation;
                }

                verticesHeight[i] = elevation;
                vertices[i] = pointOnUnitSphere * planetRadius * (1 + elevation); ;

                if (x != resolution - 1 && y != resolution - 1)
                {
                    tringles[triIndex] = i;
                    tringles[triIndex + 1] = i + resolution + 1;
                    tringles[triIndex + 2] = i + resolution;

                    tringles[triIndex + 3] = i;
                    tringles[triIndex + 4] = i + 1;
                    tringles[triIndex + 5] = i + resolution + 1;

                    triIndex += 6;
                }
            }
            
        }





    }


    float NoiseEvaluate(Vector3 pointOnUnitSphere, OctaveInformation octave)
    {

        Noise noise = new Noise();
        float noiseValue = 0;
        float frequency = octave.baseRoughness;
        float amplitude = 1;



        for (int layer = 0; layer < octave.numLayers; layer++)
        {
            float height = noise.Evaluate(pointOnUnitSphere * frequency + octave.offset);
            noiseValue += (height + 1) * 0.5f * amplitude;

            frequency *= octave.roughness;
            amplitude *= octave.persistance;
        }
        noiseValue = Mathf.Max(0, noiseValue - octave.minValue);
        noiseValue *= octave.strength;

        return noiseValue;
    }

    public void BuildMesh()
    {


        Color[] colors = new Color[resolution * resolution];
        for (int y = 0; y < resolution; y++)
        {
            for (int x = 0; x < resolution; x++)
            {

                int i = x + y * resolution;
                float height = Mathf.InverseLerp(minHeight, maxHeight, verticesHeight[i]);
                colors[i] = gradient.Evaluate(height);

            }

        }

        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = tringles;
        mesh.colors = colors;
        mesh.RecalculateNormals();

        SpawnItems();
    }

    void SpawnItems()
    {

        if (Application.isPlaying)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            foreach (ItemToSpawn item in itemsSpawner.spawnItems)
            {
                int currAmount = 0;
                while (currAmount < item.spawnNum && stopwatch.ElapsedMilliseconds < 1000)
                {
                    int randomVertices = Random.Range(0, vertices.Length);

                    float height = Mathf.InverseLerp(minHeight, maxHeight, verticesHeight[randomVertices]);

                    if (item.minSpawnHeightRatio < height && height < item.maxSpawnHeightRatio)
                    {


                        itemsSpawner.SpawnItem(item, vertices[randomVertices], itemsParent);
                        currAmount++;
                    }

                }



            }
        }
        
    }

    
}
