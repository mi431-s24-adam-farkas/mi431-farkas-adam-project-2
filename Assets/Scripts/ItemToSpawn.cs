
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[CreateAssetMenu(fileName = "Item", menuName = "ScriptableObjects/ItemSpawn", order = 2)]
public class ItemToSpawn : ScriptableObject
{


    public GameObject gameObjectToSpawn;

    public int spawnNum;

    [MinValue(0), MaxValue(1)]
    public float minSpawnHeightRatio;

    [MinValue(0), MaxValue(1)]
    public float maxSpawnHeightRatio;

    public float scale;


}
