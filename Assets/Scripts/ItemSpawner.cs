using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class ItemSpawner : MonoBehaviour
{
    [Expandable]
    public ItemToSpawn[] spawnItems;


    public void SpawnItem(ItemToSpawn item, Vector3 position, GameObject parentGO)
    {
        GameObject spawnGO = Instantiate(item.gameObjectToSpawn, parentGO.transform);
        spawnGO.transform.position = position;


        Vector3 rotation = position - transform.position ;

        spawnGO.transform.up = rotation;

        spawnGO.transform.localScale = new Vector3(item.scale, item.scale, item.scale);
    }
    
}
