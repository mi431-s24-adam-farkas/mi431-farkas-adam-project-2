using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Octave", menuName = "ScriptableObjects/OctaveInformation", order = 1)]
public class OctaveInformation : ScriptableObject
{


    public event Action OnDataChanged;

    public bool enabled = true;

    public bool useFirstLayerAsMask = false;

    public float strength = 1;

    [Range(1,8)]
    public int numLayers;
    public float baseRoughness = 1;
    public float roughness = 2;
    public float persistance = 0.5f;
    
    public Vector3 offset;

    public float minValue = 1;

    


    private void OnValidate()
    {
        // Call the data changed event if it's not null
        OnDataChanged?.Invoke();
    }

}
