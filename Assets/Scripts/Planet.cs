using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class Planet : MonoBehaviour
{
    [Range(2,256)]
    public int resolution = 10;


    [SerializeField,HideInInspector]
    MeshFilter[] meshFilters;
    TerrainFace[] terrainFaces;

    [Expandable]
    public OctaveInformation[] octaves;

    public GameObject itemsParent;

    public float planetRadius;

    public Gradient gradient;

    public ItemSpawner itemSpawner;



    private void OnEnable()
    {
        foreach(OctaveInformation octave in  octaves)
        {
            octave.OnDataChanged += HandleDataChanged;
        }
        
    }

    private void OnDisable()
    {
        foreach (OctaveInformation octave in octaves)
        {
            octave.OnDataChanged -= HandleDataChanged;
        }
    }


    public void HandleDataChanged()
    {
        OnValidate();
    }

    private void OnValidate()
    {

        if (Application.isPlaying)
        {
            foreach (Transform child in itemsParent.transform)
            {

                Destroy(child.gameObject);
            }
        }

        
        Initialize();
        GenerateMesh();
    }

    void Initialize()
    {
        if (meshFilters == null || meshFilters.Length == 0)
        {
            meshFilters = new MeshFilter[6];
        }
        
        terrainFaces = new TerrainFace[6];

        Vector3[] directions = { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back }; 

        for(int i = 0; i < meshFilters.Length; i++)
        {
            if (meshFilters[i] == null)
            {
                GameObject meshObj = new GameObject("mesh");
                meshObj.transform.parent = transform;


                meshObj.AddComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Universal Render Pipeline/Lit"));
                meshFilters[i] = meshObj.AddComponent<MeshFilter>();
                meshFilters[i].sharedMesh = new Mesh();

                
            }

            terrainFaces[i] = new TerrainFace(meshFilters[i].sharedMesh, resolution, directions[i],planetRadius, octaves,gradient,itemSpawner, itemsParent);

        }
    }

    void GenerateMesh()
    {
        foreach(TerrainFace face in terrainFaces)
        {
            face.ConstructVertices();
        }

        foreach (TerrainFace face in terrainFaces)
        {
            face.BuildMesh();
        }
    }

    void OnApplicationQuit()
    {
        if (Application.isPlaying)
        {
            
            foreach (Transform child in itemsParent.transform)
            {
                
                Destroy(child.gameObject);
            }
        }
    }
}
